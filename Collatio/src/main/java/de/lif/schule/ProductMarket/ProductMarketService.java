package de.lif.schule.ProductMarket;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class ProductMarketService {


    @Resource
    private ProductMarketRepository repo ;

    public List<ProductMarket> listAll() {
        return repo.findAll();
    }

    public void save(ProductMarket productMarket) {
        repo.save(productMarket);
    }

    public ProductMarket get(long id) {
        return repo.findById(id).get();
    }

    public void delete(long id) {
        repo.deleteById(id);
    }


}
