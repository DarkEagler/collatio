package de.lif.schule.ProductMarket.Form;


import org.springframework.stereotype.Component;

@Component
public class ProductMarketForm {

    private String marketName;
    private String categoryName;
    private String productName;
    private String productInfo;
    private double currentPrice;
    private double rabbatPrice;
    private Integer productGrammage;


    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(String productInfo) {
        this.productInfo = productInfo;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public double getRabbatPrice() {
        return rabbatPrice;
    }

    public void setRabbatPrice(double rabbatPrice) {
        this.rabbatPrice = rabbatPrice;
    }

    public Integer getProductGrammage() {
        return productGrammage;
    }

    public void setProductGrammage(Integer productGrammage) {
        this.productGrammage = productGrammage;
    }

    @Override
    public String toString() {
        return "ProductMarketForm{" +
                "marketName='" + marketName + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", productName='" + productName + '\'' +
                ", productInfo='" + productInfo + '\'' +
                ", currentPrice=" + currentPrice +
                ", rabbatPrice=" + rabbatPrice +
                ", productGrammage=" + productGrammage +
                '}';
    }
}
