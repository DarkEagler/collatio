package de.lif.schule.ProductMarket.Facade;

import de.lif.schule.ProductMarket.Form.ProductMarketForm;
import de.lif.schule.ProductMarket.ProductMarket;
import de.lif.schule.ProductMarket.ProductMarketService;
import de.lif.schule.category.Category;
import de.lif.schule.category.CategoryService;
import de.lif.schule.market.Market;
import de.lif.schule.market.MarketService;
import de.lif.schule.product.Product;
import de.lif.schule.product.ProductService;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Component
public class ProductMarketFacade {

    @Resource
    ProductService productService;
    @Resource
    CategoryService categoryService;
    @Resource
    MarketService marketService;
    @Resource
    ProductMarketService productMarketService;

    public void createMarketProduct(ProductMarketForm form) {
        // todo: CategoryMapper..
        Product product = new Product();
        Category category = new Category();
        category.setCategoryName(form.getCategoryName());
        categoryService.save(category);
        product.setCategory(category);
        product.setProductName(form.getProductName());
        product.setProductInfo(form.getProductInfo());
        product.setProductGrammage(form.getProductGrammage());
        productService.save(product);

        List<Market> markets= marketService.getMarketByName(form.getMarketName());
        Market market = new Market();
        ProductMarket productMarket = new ProductMarket();
        productMarket.setProduct(product);
        if(markets.isEmpty()){
            market.setMarketName(form.getMarketName());
            marketService.save(market);
            productMarket.setMarket(market);
        }else{
            System.out.println(markets.get(0).getMarketName());
            productMarket.setMarket(markets.get(0));
        }
        productMarket.setTimestamp(new Date());
        productMarket.setCurrentPrice(form.getCurrentPrice());
        productMarket.setRabbatPrice(form.getRabbatPrice());
        productMarketService.save(productMarket);
    }

}
