package de.lif.schule.ProductMarket;


import de.lif.schule.market.Market;
import de.lif.schule.product.Product;

import javax.persistence.*;

import java.util.Date;


@Entity
@Table
public class ProductMarket  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name="product_id",referencedColumnName = "id")
    Product product;

    @ManyToOne
    @JoinColumn(name="market_id",referencedColumnName = "id")
    Market market;

    @Column(name = "timestamp")
    Date timestamp;

    @Column(name="currentPrice")
    Double currentPrice;

    @Column(name="rabbatPrice")
    Double rabbatPrice;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(Double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public Double getRabbatPrice() {
        return rabbatPrice;
    }

    public void setRabbatPrice(Double rabbatPrice) {
        this.rabbatPrice = rabbatPrice;
    }
}
