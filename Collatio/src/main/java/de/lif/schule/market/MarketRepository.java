package de.lif.schule.market;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MarketRepository extends JpaRepository<Market, Long> {

List<Market> findByMarketName(String marketName);
}
