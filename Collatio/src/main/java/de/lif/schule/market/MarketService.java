package de.lif.schule.market;

import de.lif.schule.category.Category;
import de.lif.schule.category.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class MarketService {

    @Resource
    private MarketRepository repo ;

    public List<Market> listAll() {
        return repo.findAll();
    }

    public void save(Market market) {
        repo.save(market);
    }

    public Market get(long id) {
        return repo.findById(id).get();
    }

    public void delete(long id) {
        repo.deleteById(id);
    }

    public List<Market> getMarketByName(String marketName){
        return repo.findByMarketName(marketName);
    }
}
