package de.lif.schule.market;

import de.lif.schule.ProductMarket.ProductMarket;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="Market")
public class Market {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id", nullable = false)
    private Long id;

    @Column(name="marketName")
    private String marketName;


    @OneToMany(mappedBy = "market",cascade = CascadeType.ALL)
    private List<ProductMarket> productMarkets = new ArrayList<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public List<ProductMarket> getProductMarkets() {
        return productMarkets;
    }

    public void setProductMarkets(List<ProductMarket> productMarkets) {
        this.productMarkets = productMarkets;
    }
}
