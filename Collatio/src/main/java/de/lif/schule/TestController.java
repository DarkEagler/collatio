package de.lif.schule;

import de.lif.schule.ProductMarket.Facade.ProductMarketFacade;
import de.lif.schule.ProductMarket.Form.ProductMarketForm;
import de.lif.schule.ProductMarket.ProductMarket;
import de.lif.schule.ProductMarket.ProductMarketService;
import de.lif.schule.category.Category;
import de.lif.schule.category.CategoryService;
import de.lif.schule.market.Market;
import de.lif.schule.market.MarketService;
import de.lif.schule.product.Product;
import de.lif.schule.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.PostMapping;
import java.util.Date;
import java.util.List;


@Controller
public class TestController{

    @Autowired
    ProductService productService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    MarketService marketService;
    @Autowired
    ProductMarketService productMarketService;

    @Autowired
    ProductMarketFacade productMarketFacade;

    @PostMapping("/search")
    public String search() {
//        String productName= "Banane";
//        String categoryName= categoryService.get(productService.getByName(productName).getCategory().getId()).getCategoryName();
//        System.out.println("Das Product '"+productName+"' befindet sich in der Kategorie '"+ categoryName+"'");



        return "home";
    }

    @PostMapping("/insert")
    public String insertTestdata() {
        ProductMarketForm form = new ProductMarketForm();
        form.setCategoryName("Fleisch");
        form.setMarketName("Aldi");
        form.setCurrentPrice(1.25);
        form.setProductName("Mango");
        form.setProductInfo("Leckeres Obst das jeder essen will! schmeckt super");
        form.setProductGrammage(280);
        productMarketFacade.createMarketProduct(form);
        return "home";
    }


}
