package de.lif.schule.product;

import de.lif.schule.ProductMarket.ProductMarket;
import de.lif.schule.category.Category;
import de.lif.schule.market.Market;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id", nullable = false)
    private Long id;

    @Column(nullable = false)
    private String productName;

    @Column(name="productInfo")
    private String productInfo;

    @Column(name="productGrammage")
    private Integer productGrammage;

    @ManyToOne
    @JoinColumn(name = "category_id",referencedColumnName = "id",nullable = false)
    Category category;

    @OneToMany(mappedBy = "product",cascade = CascadeType.ALL)
    private List<ProductMarket> productMarkets= new ArrayList<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<ProductMarket> getProductMarkets() {
        return productMarkets;
    }

    public void setProductMarkets(List<ProductMarket> productMarkets) {
        this.productMarkets = productMarkets;
    }

    public Integer getProductGrammage() {
        return productGrammage;
    }

    public void setProductGrammage(Integer productGrammage) {
        this.productGrammage = productGrammage;
    }

    public String getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(String productInfo) {
        this.productInfo = productInfo;
    }
}
