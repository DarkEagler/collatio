package de.lif.schule.product;


import de.lif.schule.ProductMarket.Facade.ProductMarketFacade;
import de.lif.schule.ProductMarket.Form.ProductMarketForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class ProductController {

    @Resource
    ProductMarketFacade productMarketFacade;

    @Resource
    private ProductService service;


    @GetMapping("/")
    public String viewHomePage(Model model) {
        List<Product> listProducts = service.listAll();
        model.addAttribute("listProducts", listProducts);

        return "home";
    }


    @GetMapping("/new")
    public String showNewProductPage(Model model) {
        ProductMarketForm form=new ProductMarketForm();
        model.addAttribute("form", form);

        return "new_product";
    }


    @PostMapping("/save")
    public String saveProduct(@ModelAttribute("productMarketForm")ProductMarketForm form) {
        productMarketFacade.createMarketProduct(form);

        return "redirect:/";
    }

//
//    @GetMapping("/edit/{id}")
//    public ModelAndView showEditProductPage(@PathVariable(name = "id") int id) {
//        ModelAndView mav = new ModelAndView("edit_product");
//        Product product = service.get(id);
//        mav.addObject("product", product);
//
//        return mav;
//    }


    @GetMapping("/delete/{id}")
    public String deleteProduct(@PathVariable(name = "id") int id) {
        service.delete(id);
        return "redirect:/";
    }
}
