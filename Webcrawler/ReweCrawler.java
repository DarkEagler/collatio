package ifi11a.awppProjekt;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import ifi11a.awppProjekt.model.Product;

public class ReweCrawler implements Runnable{
	/*@TODO:
	 * 1. Catch Server Error responses (503) 
	 * 2. Handle Server Error�s
	 * 3. Reload http Fetch when jsoup throws Error
	*/
	private String startingHTTP = "https://shop.rewe.de/";
	private int siteCounter = 1;
	private String httpEntry = startingHTTP+"productList?page="+siteCounter;
	private String marketName ="REWE";
	private int maxSiteCounter;
	private Document html;
	
	public ReweCrawler() {
		this.html = loadHTML(httpEntry);
		this.maxSiteCounter = getMaxSites(html);
	}
	@Override
	public void run() {
//		httpEntry = startingHTTP+"productList?page="+siteCounter;
		do {
//			System.out.println("httpEntry -> " + httpEntry);
//			System.out.println("siteCounter -> " + siteCounter);
		if(siteCounter > 1) {
			html = loadHTML(startingHTTP+"productList?page="+siteCounter);
		}
		Elements divs = getProductNodes(html);
		List<Element> rawProductList;
		rawProductList = getRawProducts(divs);
		List<Product> products = createProduct(rawProductList);
		System.out.println("Done Page " + siteCounter + "\n\n\n");
		siteCounter++;
		}
		while(siteCounter < maxSiteCounter);
	}

	
	private int getMaxSites(Document html) {
		Node node = html.getElementsByClass("search-service-paginationPage search-service-paginationPageLink").last();
		int maxPage = Integer.parseInt(node.childNode(0).toString());
		return maxPage;
	}


	private List<Product> createProduct(List<Element> rawProductList) {
		List<Product> p = new LinkedList<Product>();
		for(Element e : rawProductList) {
			p.add(ProductBuilder.createProduct(e, this.marketName,this.startingHTTP));
		}
		System.out.println(p.size());
		return convertToArrayList(p);
	}


	private List<Product> convertToArrayList(List<Product> p) {
//		return new ArrayList<Product>().addAll(p);
		List<Product> list = new ArrayList<Product>();
		list.addAll(p);
		return list;
	}


	private List<Element> getRawProducts(Elements el) {
		List<Element> rawProductList = new LinkedList<Element>();
		for(Element e: el) {
			if(e.toString().contains("search-service-product")) {
				if(e.toString().startsWith("<div class=\"search-service-product\"")) {
					rawProductList.add(e);
				}
			}
		}
		return rawProductList;
	}

	private Elements getProductNodes(Document html) {
		Elements el;
		el = html.select("div");
		return el;
	}

	private Document loadHTML(String httpEntry2) {
		System.out.println("HTTPEntry -> " +httpEntry2);
		Document htmlText = null;
		try {
			htmlText = Jsoup.parse(new URL(httpEntry2).openStream(), "UTF-8", httpEntry2);
		} catch (IOException e) {
			e.printStackTrace();
		}  
        return htmlText;
	}

}
