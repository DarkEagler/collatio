package ifi11a.awppProjekt;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import ifi11a.awppProjekt.model.Product;

public class ProductBuilder {

	public static Product createProduct(Element e, String market, String startingHref) {
		Element dv = getDetailView(e, startingHref);
		String productInfo = getProductInfos(dv);
		String productName = getProductNameFromDetailView(dv);
		String marketName = market; // DONE
		String productAmount = getProductAmount(e); // DONE
		double currentPrice = getCurrentPrice(e)+0.00; // DONE
		int productGrammage = getProductGrammage(e); // DONE
		String categoryName = getCategoryName(dv);

		Product p;
		if (isOffer(e)) {
			Date endDate = getOfferEndDate(e); // DONE
			double offerPrice = getOfferPrice(e)+0.00; // DONE
			currentPrice = getOfferOriginalPrice(e)+0.00; // DONE
			p = new Product(productInfo, productName, marketName, productAmount, currentPrice, productGrammage, endDate,
					offerPrice, categoryName);
		} else {
			p = new Product(productInfo, productName, marketName, productAmount, currentPrice, productGrammage,
					categoryName);
		}
		p.info();
		System.out.println();
		return p;
	}

	private static String getCategoryName(Element dv) {
		System.out.println();
		Element categoryElement = dv.getElementsByClass("lr-breadcrumbs").first();
		Node leadingCategory = categoryElement.getElementsByTag("a").first();
		String categoryTextRaw = leadingCategory.attributes().toString();
		categoryTextRaw = categoryTextRaw.substring(10);
		String categoryTextClean = categoryTextRaw.substring(0, categoryTextRaw.indexOf('/'));
		return categoryTextClean;
	}

	private static String getProductNameFromDetailView(Element dv) {
		Node productName = dv.getElementsByTag("h1").first();
		String productNameText = productName.childNode(0).toString();
		return productNameText;
	}

	private static Element getDetailView(Element e, String startHref) {
		Document html = null;
		Node href = e.getElementsByTag("a").first();
		String link = href.attributes().toString();
		link = link.substring(7);
		link = link.substring(0, link.length() - 1);
		String url = startHref + link;
		try {
			html = Jsoup.parse(new URL(url).openStream(), "UTF-8", url);
		} catch (MalformedURLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
		return html;
	}

	private static double getOfferOriginalPrice(Element e) {
		Element offer = e.getElementsByClass("search-service-productOfferOriginalPrice").first();
		Node offerOriginalPrice = offer.childNode(0);
		String offerOriginalPriceText = offerOriginalPrice.toString().substring(1);
		offerOriginalPriceText = offerOriginalPriceText.substring(0, offerOriginalPriceText.length() - 1);
		offerOriginalPriceText = replaceKomma(offerOriginalPriceText);
		return roundTo2Decimals(Double.parseDouble(offerOriginalPriceText));
	}

	private static boolean isOffer(Element e) {
		Elements el = e.getElementsByClass("search-service-productOffer");
		return el.size() > 0;
	}

	private static double getOfferPrice(Element e) {
		Element offer = e.getElementsByClass("search-service-productOffer").first();
		Node offerPrice = offer.childNode(1).childNode(0).childNode(0);
		String offerPriceText = offerPrice.toString().substring(1, offerPrice.toString().length() - 1);
		offerPriceText = replaceKomma(offerPriceText);
		return roundTo2Decimals(Double.parseDouble(offerPriceText));
	}

	private static Date getOfferEndDate(Element e) {
		Element offer = e.getElementsByClass("search-service-productOffer").get(0);
		String endDateText = offer.childNode(0).childNode(0).childNode(2).toString();
		try {
			return new SimpleDateFormat("dd.MM.yyyy").parse(endDateText);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}

		return null;
	}

	private static int getProductGrammage(Element e) {
		/*
		 * 250g 1l 2kg 1 St�ck ca 200 g
		 */
		Node grammageNode = getGrammageNode(e);
		String nodeText = grammageNode.toString().substring(1);
		nodeText = clearBrackets(nodeText);
		if (nodeText.contains("l")) {
			String amount = cutCharacters(nodeText);
			amount += "000";
			return Integer.parseInt(amount);
		} else if (nodeText.contains("ck")) {
			// 1 St�ck ca. 200g
			if (nodeText.contains("g")) {
				if (nodeText.contains("kg")) {
					System.out.println("KILOGRAMMMMASMD -> " + nodeText);
				} else {
					int positionOfG = nodeText.indexOf('g');
					String rawPrice = nodeText.substring(positionOfG - 5, positionOfG);
					rawPrice = rawPrice.replaceAll("[(\\s)]", "");
					return Integer.parseInt(rawPrice);
				}
			} else {
				return -1;
			}
		} else if (nodeText.contains("" + 'g')) {
			// 280g (1 kg = 17,82 �)
			String grammageText = "";
			int grammage;
			if (nodeText.contains("k") && !nodeText.contains("(")) {
				grammageText = nodeText.substring(0, nodeText.indexOf('k', 0));
				grammage = Integer.parseInt(grammageText.replaceAll(",", "")+"0");
//				grammage *= 1000;
			} else {
				grammageText = nodeText.substring(0, nodeText.indexOf('g', 0));
				grammage = Integer.parseInt(grammageText);

			}

			return grammage;
		}
		return -1;
	}

	private static String cutCharacters(String nodeText) {
		return nodeText.replaceAll("[\\D]", "");
	}

	private static String clearBrackets(String nodeText) {
		return nodeText.replaceAll("(\\(.*\\))", "");
	}

	private static double getCurrentPrice(Element e) {
		Elements elements = e.getElementsByClass("search-service-productPrice");
		if (elements.size() < 1) {
			return -1;
		}
		Element el = e.getElementsByClass("search-service-productPrice").get(0);
		Node priceNode = el.childNode(0);
		String price = priceNode.toString().substring(1, priceNode.toString().length() - 1);
		price = replaceKomma(price);
		return roundTo2Decimals(Double.parseDouble(price));
	}

	private static double roundTo2Decimals(double val) {
		DecimalFormat df2 = new DecimalFormat("###.00");
		Double endprice = Double.valueOf(df2.format(val).replace(',', '.'));
		return endprice;
	}

	private static String getProductAmount(Element e) {
		Node grammage = getGrammageNode(e);
		String grammageText = grammage.toString().substring(1);
		if (grammageText.contains("St�ck")) {
			return grammageText.substring(0, grammageText.indexOf(' '));
		}
		return null;
	}

	/**
	 * @param e
	 * @return
	 */
	private static Node getGrammageNode(Element e) {
		Element grammage = e.getElementsByClass("search-service-productGrammage").get(0);
		Node grammageNode = grammage.childNode(0).childNode(0);
		return grammageNode;
	}

	private static String getProductInfos(Element e) {
		Element infoElement = e.getElementsByClass("pdr-AttributeGroup").first();
		Node infoNode = infoElement.childNode(0).childNode(0);
		String infoText = infoNode.toString();
		// infoText = infoText.replaceAll("[&amp]", "&");
		return infoText;
	}

	private static String replaceKomma(String s) {
		return s.replaceAll(",", ".");
	}
}
