package ifi11a.awppProjekt.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Product {

	private String productInfo;
	private String productName;
	private String marketName;
	private String categoryName;
	private String productAmount;
	private double currentPrice;
	private double rabbatPrice;
	private int productGrammage;
	private Map<Object, Object> offerinformations;

	/*
	 * Products who aren�t in offer
	 */
	public Product(String productInfo, String productName, String marketName, String productAmount, double currentPrice,
			int productGrammage,String categoryName) {
		this.productInfo = productInfo;
		this.productName = productName;
		this.marketName = marketName;
		this.productAmount = productAmount;
		this.currentPrice = currentPrice;
		this.productGrammage = productGrammage;
		this.offerinformations = null;
		this.categoryName = categoryName;
		this.rabbatPrice = -1;
	}
	
	/*
	 * If Product is offered
	 */
	public Product(String productInfo, String productName, String marketName, String productAmount, double currentPrice,
			int productGrammage, Date endDate, double offerPrice, String categoryName) {
		this.productInfo = productInfo;
		this.productName = productName;
		this.marketName = marketName;
		this.productAmount = productAmount;
		this.currentPrice = currentPrice;
		this.productGrammage = productGrammage;
		this.rabbatPrice = offerPrice;
		this.offerinformations = new HashMap<>();
		this.offerinformations.put("Date", endDate);
		this.offerinformations.put("rabbatPrice", rabbatPrice);
		this.categoryName = categoryName;
	}

	public String getProductName() {
		return this.productName;
	}

	public void info() {
		System.out.println("marketName -> " + this.marketName);
		System.out.println("categoryName -> " + this.categoryName);
		System.out.println("productName -> " + this.productName);
		System.out.println("currentPrice -> " + this.currentPrice);
		System.out.println("productAmount -> " + this.productAmount);
		System.out.println("productGrammage -> " + this.productGrammage);
		System.out.println("offerinformations -> " + this.offerinformations);
		System.out.println("productInfo -> " + this.productInfo);
	}
	public String getProductInfo() {
		return this.productInfo;
	}

	public String getMarketName() {
		return this.marketName;
	}

	public String getCategoryName() {
		return this.categoryName;
	}

	public String getProductAmount() {
		return this.productAmount;
	}

	public double getCurrentPrice() {
		return this.currentPrice;
	}

	public double getRabbatPrice() {
		return this.rabbatPrice;
	}

	public int getProductGrammage() {
		return this.productGrammage;
	}

	public Map<Object, Object> getOfferinformations() {
		return this.offerinformations;
	}
}
